# Imap and Pop with XOAuth2 authentication

### Requirements
- [php-imap2](https://github.com/javanile/php-imap2) with XOAUTH2 support, install with ```composer```. 
- [PHPMailer](https://github.com/PHPMailer/PHPMailer) with XOAUTH2 support, install with ```composer```.

See below for ```composer``` installation steps if needed.

#### Ubuntu/Debian composer installation steps
```
sudo apt install curl php-cli php-mbstring git unzip
curl -sS https://getcomposer.org/installer -o composer-setup.php
HASH=`curl -sS https://composer.github.io/installer.sig`
php -r "if (hash_file('SHA384', 'composer-setup.php') === '$HASH') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
sudo php composer-setup.php --install-dir=/usr/local/bin --filename=composer
composer
php -r "unlink('composer-setup.php');"
```

#### CentOS7 composer installation steps
```
yum install php-cli php-zip wget unzip
php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
HASH="$(wget -q -O - https://composer.github.io/installer.sig)"
php -r "if (hash_file('SHA384', 'composer-setup.php') === '$HASH') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
php composer-setup.php --install-dir=/usr/local/bin --filename=composer
composer
php -r "unlink('composer-setup.php');"
```

### Running the sample
Before first run, install requirements with: 
```
composer install
```

Then running the samples:
- ```php imap_client.php```
- ```php smtp_client.php```


