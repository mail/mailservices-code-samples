<?php
include("oauth2_flow.php");
require __DIR__ . '/vendor/autoload.php';
require __DIR__ . '/Cern365OAuthTokenProvider.php';

//Import PHPMailer classes into the global namespace
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\OAuth;
// Import Cern specific Oauth class
use PHPMailer\PHPMailer\Cern365OAuthTokenProvider;

// Aquire token
$access_token = get_access_token();
// Extract username from token
$username = get_username_from_access_token($access_token);


$mail = new PHPMailer(true);
$mail->isSMTP();
//Enable SMTP debugging
//SMTP::DEBUG_OFF = off (for production use)
//SMTP::DEBUG_CLIENT = client messages
//SMTP::DEBUG_SERVER = client and server messages
$mail->SMTPDebug = SMTP::DEBUG_OFF;
//Set the hostname of the mail server
$mail->Host = "outlook.office365.com";
//Set the SMTP port number - 587 for authenticated TLS, a.k.a. RFC4409 SMTP submission
$mail->Port = 587;
//Set the encryption mechanism to use - STARTTLS or SMTPS
$mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;
//Whether to use SMTP authentication
$mail->SMTPAuth = true;
//Set AuthType to use XOAUTH2
$mail->AuthType = 'XOAUTH2';

//Option 2: Another OAuth library as OAuth2 token provider
//Set up the other oauth library as per its documentation
//Then create the wrapper class that implementations OAuthTokenProvider
$oauthTokenProvider = new Cern365OAuthTokenProvider([
    'username' => $username,
    'access_token' => $access_token
]);
//Pass the implementation of OAuthTokenProvider to PHPMailer
$mail->setOAuth($oauthTokenProvider);
//End Option 2

// This must match the authenticated user otherwise mail will be rejected (SMTP Error: data not accepted.)
$mail->From = $username;
// To
$mail->addAddress("noreply@cern.ch"); // UPDATE ACCORDINGLY

$mail->Subject = "Testing SMTP XOauth2 in PHP";
//$mail->msgHTML("<p>Hello world</p>");
$mail->Body = "Hello world";

if (!$mail->send()) {
    echo "ERROR\n";
} else {
    echo "Mail sent successfully\n";
}

?>