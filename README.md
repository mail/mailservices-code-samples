# Mail Service Code Samples

## Oauth IMAP, POP, SMTP and Graph API samples
- Python and .NET samples, using Microsoft ```msal``` library.
- PHP samples.
- Java samples.
- Golang samples.
- [Official documentation](https://docs.microsoft.com/en-us/exchange/client-developer/legacy-protocols/how-to-authenticate-an-imap-pop-smtp-application-by-using-oauth)
