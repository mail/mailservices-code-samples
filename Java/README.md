# IMAP, POP and SMTP with XOAUTH2

## Setup
Needs MSAL for Java:
- https://github.com/AzureAD/microsoft-authentication-library-for-java
- https://github.com/AzureAD/microsoft-authentication-library-for-java/wiki

Install dependancies with Maven (https://maven.apache.org/guides/getting-started/maven-in-five-minutes.html)

Add to ```pom.xml``` these dependencies:
- MSAL for Java
```
<dependency>
    <groupId>com.microsoft.azure</groupId>
    <artifactId>msal4j</artifactId>
    <version>1.14.0-beta</version>
</dependency>
```
- JavaMail
```
<dependency>
    <groupId>com.sun.mail</groupId>
    <artifactId>jakarta.mail</artifactId>
    <version>2.0.1</version>
</dependency>
```

Note: it might work with the older Javamail version, except for POP:
```
<dependency>
    <groupId>com.sun.mail</groupId>
    <artifactId>javax.mail</artifactId>
    <version>1.6.2</version>
</dependency>
```

## Run
Run samples:
```
mvn compile
mvn package
mvn dependency:resolve
mvn dependency:copy-dependencies
java -cp 'target/oauth2-imap-1.0-SNAPSHOT.jar:target/dependency/*' ch.cern.app.App
```

## Notes

### Scopes
Requested scopes must be on ```https://outlook.office.com```, not on ```https://graph.microsoft.com``` despite the App registration in Azure.

- IMAP Scope: ```https://outlook.office.com/IMAP.AccessAsUser.All```
- POP Scope: ```https://outlook.office.com/POP.AccessAsUser.All```
- SMTP Scope: ```https://outlook.office.com/SMTP.Send```

### Authority
Specify the tenant domain in authority:
```https://login.microsoftonline.com/cern.ch```

As the app is restricted to enterprise users, the standard authority uri fails (cannot indentify tenant)
```https://login.microsoftonline.com/common```

### Oauth token to XOauth2
https://docs.microsoft.com/en-us/exchange/client-developer/legacy-protocols/how-to-authenticate-an-imap-pop-smtp-application-by-using-oauth#sasl-xoauth2
```
base64("user=" + userName + "^Aauth=Bearer " + accessToken + "^A^A")
```
