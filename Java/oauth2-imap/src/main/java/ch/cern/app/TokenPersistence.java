package ch.cern.app;

import com.microsoft.aad.msal4j.ITokenCacheAccessAspect;
import com.microsoft.aad.msal4j.ITokenCacheAccessContext;

import java.nio.file.Files;
import java.nio.file.attribute.*;
import java.nio.file.Paths;

public class TokenPersistence implements ITokenCacheAccessAspect {
    private String data;
    private String fileName;

    public TokenPersistence(String fileName) {
        this.fileName = fileName;
        this.data = readDataFromFile(fileName);
    }

    @Override
    public void beforeCacheAccess(ITokenCacheAccessContext iTokenCacheAccessContext) {
        iTokenCacheAccessContext.tokenCache().deserialize(data);
    }

    @Override
    public void afterCacheAccess(ITokenCacheAccessContext iTokenCacheAccessContext) {
        this.data = iTokenCacheAccessContext.tokenCache().serialize();
        // logic to write changes to file
        saveDataToFile(this.data);
    }

    private String readDataFromFile(String fileName) {
        try {
            return new String(Files.readAllBytes(Paths.get(fileName)));
        } catch (Exception ex){
            System.out.println("Error reading data from file: " + ex.getMessage());
            //throw new RuntimeException(ex);
        }
        return null;
    }

    private void saveDataToFile(String data) {
        try {
            Files.write(Paths.get(fileName), data.getBytes());
            Files.setPosixFilePermissions(Paths.get(fileName), PosixFilePermissions.fromString("rw-------"));

        } catch (Exception ex){
            System.out.println("Error writing data to file: " + ex.getMessage());
            //throw new RuntimeException(ex);
        }
    }
}