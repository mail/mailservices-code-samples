package ch.cern.app;

import com.microsoft.aad.msal4j.IAuthenticationResult;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        System.out.println( "IMAP with XOAUTH2 Sample" );
        Oauth2Flow o = new Oauth2Flow();
        IAuthenticationResult token = o.getAccessToken();

        ImapClient ic = new ImapClient();
        ic.ImapConnect(token.account().username(), token.accessToken());

        PopClient pc = new PopClient();
        pc.PopConnect(token.account().username(), token.accessToken());

        SmtpClient sc = new SmtpClient();
        sc.SmtpConnect(token.account().username(), token.accessToken());

    }
}
