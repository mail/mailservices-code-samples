# IMAP, POP and SMTP with XOAUTH2

## Setup
```
pip install -r requirements.txt
```
Edit eventually ```config.py``` for ```TOKEN_CACHE_FILE``` path.

Run samples:
```
python -m oauth2_imap
python -m oauth2_pop
python -m oauth2_smtp
```

## Notes

### Scopes
Requested scopes must be on ```https://outlook.office.com```, not on ```https://graph.microsoft.com``` despite the App registration in Azure.

- IMAP Scope: ```https://outlook.office.com/IMAP.AccessAsUser.All```
- POP Scope: ```https://outlook.office.com/POP.AccessAsUser.All```
- SMTP Scope: ```https://outlook.office.com/SMTP.Send```

### Authority
Specify the tenant domain in authority:
```https://login.microsoftonline.com/cern.ch```

As the app is restricted to enterprise users, the standard authority uri fails (cannot indentify tenant)
```https://login.microsoftonline.com/common```

### Oauth token to XOauth2
https://docs.microsoft.com/en-us/exchange/client-developer/legacy-protocols/how-to-authenticate-an-imap-pop-smtp-application-by-using-oauth#sasl-xoauth2
```
base64("user=" + userName + "^Aauth=Bearer " + accessToken + "^A^A")
```

Note: IMAP apparently does not want a base64 encoded token, while POP and SMTP requires it.
