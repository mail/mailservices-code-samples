# Graph API with XOAUTH2

## Setup
```
pip install -r requirements.txt
```
Edit eventually ```config.py``` for ```TOKEN_CACHE_FILE``` path.

Run samples:
```
python -m oauth2_graph
```

## Notes

### Scopes
Requested scopes must be:

- Graph Scope: ```https://graph.microsoft.com/.default```

### Authority
Specify the tenant domain in authority:
```https://login.microsoftonline.com/cern.ch```

As the app is restricted to enterprise users, the standard authority uri fails (cannot indentify tenant)
```https://login.microsoftonline.com/common```

### Graph API methods
More methods can be found here:

- [https://github.com/domainaware/parsedmarc/blob/master/parsedmarc/mail/graph.py](https://github.com/domainaware/parsedmarc/blob/master/parsedmarc/mail/graph.py)
