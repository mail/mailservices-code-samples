"""oauth2-graph Entrypoint."""
from oauth2_graph.config import load_config
from oauth2_graph.graph_client import GraphClient


if __name__ == "__main__":
    conf = load_config()
    z = GraphClient(conf)
    z.process_mailbox()
