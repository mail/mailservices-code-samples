# Golang XOauth2 sample

## Setup
```
go get -u github.com/AzureAD/microsoft-authentication-library-for-go/
```

## Run
```
go run ./ acessToken
```

## Notes

### Scopes
Requested scopes must be on ```https://outlook.office.com```, not on ```https://graph.microsoft.com``` despite the App registration in Azure.

- IMAP Scope: ```https://outlook.office.com/IMAP.AccessAsUser.All```
- POP Scope: ```https://outlook.office.com/POP.AccessAsUser.All```
- SMTP Scope: ```https://outlook.office.com/SMTP.Send```

### Authority
Specify the tenant domain in authority:
```https://login.microsoftonline.com/cern.ch```

As the app is restricted to enterprise users, the standard authority uri fails (cannot indentify tenant)
```https://login.microsoftonline.com/common```

- [Official MSAL Go Repository](https://github.com/AzureAD/microsoft-authentication-library-for-go/tree/dev)